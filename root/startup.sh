#! /bin/sh

trap "umount -a; killall sleep; exit" TERM INT

cat /mounts | tr '\\' '/' | while read LINE; do
  DISK=$(echo $LINE | cut -d ',' -f1)
  LOCAL=$(echo $LINE | cut -d ',' -f2)
  mkdir -p "/disks/${LOCAL}"
  mount "${DISK}" "/disks/${LOCAL}"
done

sleep 2147483647d &
wait "$!"
